/* hello_sdl2.c
 * Eryn Wells <eryn@erynwells.me>
 */

#include <cassert>
#include <cstdio>
#include <map>
#include <memory>
#include <string>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>


static const int WINDOW_WIDTH = 640;
static const int WINDOW_HEIGHT = 480;


struct Texture
{
    Texture(const std::string &path)
        : _path(path),
          _texture(NULL)
    { }

    ~Texture()
    {
        if (_texture) {
            SDL_DestroyTexture(_texture);
        }
    }

    bool load(SDL_Renderer *renderer)
    {
        SDL_Surface *surface = IMG_Load(_path.c_str());
        if (!surface) {
            printf("Unable to create surface with image: path = %s; error = %s\n", _path.c_str(), SDL_GetError());
            return false;
        }

        // Color key the image. I'm not sure what this does.
        SDL_SetColorKey(surface, SDL_TRUE, SDL_MapRGB(surface->format, 0, 0xFF, 0xFF));

        _texture = SDL_CreateTextureFromSurface(renderer, surface);
        assert(_texture);

        _width = surface->w;
        _height = surface->h;

        SDL_FreeSurface(surface);
        return true;
    }

    SDL_Texture *get_texture() { return _texture; }
    int get_width() const { return _width; }
    int get_height() const { return _height; }

private:
    std::string _path;
    SDL_Texture *_texture;
    int _width, _height;
};


struct Window
{
    typedef std::shared_ptr<Window> Ptr;

    Window(const std::string& title,
           int x = SDL_WINDOWPOS_UNDEFINED,
           int y = SDL_WINDOWPOS_UNDEFINED,
           int width = WINDOW_WIDTH,
           int height = WINDOW_HEIGHT)
        : _window(SDL_CreateWindow(title.c_str(), x, y, width, height, SDL_WINDOW_RESIZABLE)),
          _renderer(SDL_CreateRenderer(_window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC))
    {
        assert(_window);
        assert(_renderer);
    }

    ~Window()
    {
        SDL_DestroyRenderer(_renderer);
        _renderer = NULL;
        SDL_DestroyWindow(_window);
        _window = NULL;
    }

    SDL_Window *get_window() { return _window; }
    SDL_Renderer *get_renderer() { return _renderer; }
    SDL_Surface *get_surface() { return SDL_GetWindowSurface(_window); }

    Uint32 get_id() const { return SDL_GetWindowID(_window); }

    void blit(SDL_Surface *surface) { SDL_BlitSurface(surface, NULL, get_surface(), NULL); }

    void clear()
    {
        SDL_RenderClear(_renderer);
    }

    void update()
    {
        SDL_RenderPresent(_renderer);
    }

    void render(Texture *texture, SDL_Rect *clip)
    {
        SDL_Rect quad = {0, 0, texture->get_width(), texture->get_height()};

        if (clip) {
            quad.w = clip->w;
            quad.h = clip->h;
        }

        SDL_RenderCopyEx(_renderer, texture->get_texture(), clip, &quad, 0.0, NULL, SDL_FLIP_NONE);
    }

    void handle_window_event(const SDL_Event& event)
    {
        assert(event.type == SDL_WINDOWEVENT);
        switch (event.window.event) {
            case SDL_WINDOWEVENT_RESIZED:
                printf("Window %d resized to (%d, %d)\n", event.window.windowID, event.window.data1, event.window.data2);
                break;
            default:
                break;
        }
    }

private:
    SDL_Window *_window;
    SDL_Renderer *_renderer;
};


struct WindowManager
{

    WindowManager()
        : _windows()
    {}

    ~WindowManager()
    { }

    void add_window(Window::Ptr window)
    {
        const unsigned winId = window->get_id();
        if (_windows.find(winId) != _windows.end()) {
            return;
        }
        _windows[winId] = window;
    }

    void dispatch_window_event(SDL_Event& event)
    {
        assert(event.type == SDL_WINDOWEVENT);
        const unsigned int winId = event.window.windowID;
        if (_windows.find(winId) != _windows.end()) {
            _windows[winId]->handle_window_event(event);
        }
    }

private:
    typedef std::map<int, Window::Ptr> WindowMap;

    WindowMap _windows;
};


int
main(int argc,
     char *argv[])
{
    WindowManager windowManager;
    Window::Ptr window;
    Texture *texture = NULL;

    const unsigned img_types = IMG_INIT_PNG | IMG_INIT_JPG;

    // Event handling
    bool quit = false;
    bool isDragging = false;
    SDL_Event ev;

    SDL_Rect camera = { 0, 0, WINDOW_WIDTH, WINDOW_HEIGHT };

    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("Couldn't initialize SDL: error = %s\n", SDL_GetError());
        goto cleanup;
    }
    if ((IMG_Init(img_types) & img_types) != img_types) {
        printf("Couldn't initialize SDL IMG library: error = %s\n", SDL_GetError());
        goto cleanup;
    }

    window.reset(new Window("SDL Tutorial"));
    windowManager.add_window(window);

    if (argc < 2) {
        goto cleanup;
    }

    texture = new Texture(argv[1]);
    if (!texture->load(window->get_renderer())) {
        goto cleanup;
    }

    do {
        window->clear();
        window->render(texture, &camera);
        window->update();

        while (SDL_PollEvent(&ev) != 0) {
            switch (ev.type) {
                case SDL_MOUSEWHEEL:
                    printf("Mouse scrolled: delta = (%d, %d)\n", ev.wheel.x, ev.wheel.y);
                    camera.x = camera.x - 8 * ev.wheel.x;
                    camera.y = camera.y + 8 * ev.wheel.y;
                    break;
                case SDL_MOUSEBUTTONDOWN:
                case SDL_MOUSEBUTTONUP:
                    if (ev.button.button == SDL_BUTTON_LEFT) {
                        isDragging = ev.button.state == SDL_PRESSED;
                    }
                    printf("Left mouse button %s\n", isDragging ? "pressed" : "released");
                    break;
                case SDL_WINDOWEVENT:
                    window->handle_window_event(ev);
                    break;
                case SDL_QUIT:
                    quit = true;
                    printf("Quitting...\n");
                    break;
                default:
                    break;
            }
        }

        // Clean up camera bounds
        if (camera.x < 0) {
            camera.x = 0;
        }
        if (camera.y < 0) {
            camera.y = 0;
        }
        if (camera.x > texture->get_width() - camera.w) {
            camera.x = texture->get_width() - camera.w;
        }
        if (camera.y > texture->get_height() - camera.h) {
            camera.y = texture->get_height() - camera.h;
        }
    } while (!quit);

cleanup:
    IMG_Quit();
    SDL_Quit();
    return 0;
}
